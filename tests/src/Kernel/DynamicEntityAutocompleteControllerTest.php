<?php

namespace Drupal\Tests\dynamic_entity_autocomplete\Kernel;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;
use Drupal\dynamic_entity_autocomplete\Controller\DynamicEntityAutocompleteController;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\node\Entity\Node;
use Drupal\Tests\token\Kernel\KernelTestBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Test the dynamic entity autocomplete controller.
 *
 * @group dymamic_entity_autocomplete
 */
class DynamicEntityAutocompleteControllerTest extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = [
    'dynamic_entity_autocomplete',
    'entity_test',
    'node',
    'user',
  ];

  /**
   * The keyvalue store containing the autocomplete settings.s
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('entity_test');

    $this->keyValue = $this->container->get('keyvalue')->get('dynamic_entity_autocomplete');
  }

  /**
   * Test the controller.
   */
  public function testController() {

    // Some test entities to execute searches against.
    Node::create([
      'title' => 'Foo',
      'type' => 'test',
    ])->save();
    EntityTest::create([
      'name' => 'Foooooo'
    ])->save();

    // String matches will return both entities.
    $this->assertEquals([
      [
        'label' => 'Foo',
        'value' => 'Foo (node:1)',
      ],
      [
        'label' => 'Foooooo',
        'value' => 'Foooooo (entity_test:1)',
      ],
    ], $this->executeSearch('Foo'));

    // No matches will be empty.
    $this->assertEquals([], $this->executeSearch('Bar'));
  }

  /**
   * Execute a search on the endpoint.
   *
   * @param string $text
   *   The text to search with.
   * @return array
   *   The decoded JSON response.
   */
  protected function executeSearch($text) {
    // Some selection settings the route will use.
    $settings = [
      'node' => [
        'selection_settings' => [],
        'selection_handler' => 'default',
      ],
      'entity_test' => [
        'selection_settings' => [],
        'selection_handler' => 'default',
      ],
    ];
    // Setup the state which the render element creates before using the route.
    $autocomplete_data = serialize($settings);
    $selection_settings_key = Crypt::hmacBase64($autocomplete_data, Settings::getHashSalt());
    $this->keyValue->set($selection_settings_key, $settings);

    $controller = new DynamicEntityAutocompleteController($this->container->get('entity.autocomplete_matcher'), $this->keyValue);
    $request = Request::create('test', 'GET', ['q' => $text]);

    return json_decode($controller->handleAutocomplete($request, $selection_settings_key)->getContent(), TRUE);
  }

}
