<?php

namespace Drupal\Tests\dynamic_entity_autocomplete\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Test the autocomplete render element.
 *
 * @group dymamic_entity_autocomplete
 */
class DynamicEntityAutocompleteElementTest extends KernelTestBase implements FormInterface {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = [
    'dynamic_entity_autocomplete',
    'entity_test',
    'node',
    'user',
  ];

  /**
   * @var \Drupal\node\NodeInterface
   */
  protected $testNode;

  /**
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $testEntityTest;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('entity_test');

    $this->testNode = Node::create([
      'title' => 'Bar',
      'type' => 'test',
    ]);
    $this->testNode->save();
    $this->testEntityTest = EntityTest::create([
      'name' => 'Foo',
    ]);
    $this->testEntityTest->save();
  }

  /**
   * Test the autocomplete element.
   */
  public function testAutocompleteElement() {
    // Test submitting a single entity.
    $single_entity = $this->submitFormWithValue(['test_element' => 'Bar (node:1)'], 'test_element');
    $this->assertCount(1, $single_entity);
    $this->assertEquals($this->testNode->id(), $single_entity[0]['entity']->id());

    // Test multiple entities.
    $multiple_entities = $this->submitFormWithValue(['test_element' => 'Bar (node:1), Foo (entity_test:1)'], 'test_element');
    $this->assertCount(2, $multiple_entities);

    // Test getting the return value of the default entities.
    $default_entities = $this->submitFormWithValue([], 'test_with_default');
    $this->assertCount(2, $default_entities);
    $this->assertInstanceOf(EntityInterface::class, $default_entities[0]['entity']);
    $this->assertInstanceOf(EntityInterface::class, $default_entities[1]['entity']);
  }

  /**
   * Submit the form with a value for the dynamic autocomplete.
   *
   * @param string $values
   *   Form values to submit
   * @param string $key
   *   The key to return from the form state.
   *
   * @return array
   *   The return value from the input.
   */
  public function submitFormWithValue($values, $key) {
    $form_state = (new FormState())
      ->setValues($values);
    $form_builder = $this->container->get('form_builder');
    $form_builder->submitForm($this, $form_state);
    return $form_state->getValue($key);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $target_types = [
      'node' => [
        'selection_handler' => 'default',
        'selection_settings' => [],
      ],
      'entity_test' => [
        'selection_handler' => 'default',
        'selection_settings' => [],
      ],
    ];
    $form['test_element'] = [
      '#title' => 'Foo',
      '#type' => 'dynamic_entity_autocomplete',
      '#target_types' => $target_types,
    ];
    $form['test_with_default'] = [
      '#title' => 'Foo',
      '#type' => 'dynamic_entity_autocomplete',
      '#target_types' => $target_types,
      '#default_value' => [
        $this->testNode,
        $this->testEntityTest,
      ],
    ];
    $form['test_with_value'] = [
      '#title' => 'Foo',
      '#type' => 'dynamic_entity_autocomplete',
      '#target_types' => $target_types,
      '#value' => [
        $this->testNode,
        $this->testEntityTest,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
